using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Context;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        
        private readonly ApiContext _context;

        public VendedorController( ApiContext ctx )
        {
            _context = ctx;
        }

        [HttpPost("CadastroVendedor")]
        public IActionResult CadastrarVendedor(Vendedor vendedor)
        {
            _context.Add(vendedor);
            _context.SaveChanges();
            return Ok(vendedor);
        }

        [HttpGet("ConsultaVendedorCad")]
        public IActionResult ListarVendedores()
        {
            
            var id = _context.Vendedores.ToList();
            return Ok(id);
        }
    }
}