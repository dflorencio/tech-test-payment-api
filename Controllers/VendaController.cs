using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Context;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using Microsoft.EntityFrameworkCore;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly ApiContext _context;
        public VendaController( ApiContext ctx )
        {
            _context = ctx;
        }

         private bool podeAtualizar(StatusVenda statusAnterior, StatusVenda statusNovo)
        {
            if (statusAnterior == StatusVenda.Aguardando_Pagamento)
            {
                if (statusNovo == StatusVenda.Pagamento_Aprovado)
                    return true;
                if (statusNovo == StatusVenda.Cancelada)
                    return true;
            }

            if (statusAnterior == StatusVenda.Pagamento_Aprovado)
            {
                if (statusNovo == StatusVenda.Enviado_Transportadora)
                    return true;
                if (statusNovo == StatusVenda.Cancelada)
                    return true;
            }

            if (statusAnterior == StatusVenda.Enviado_Transportadora
                && statusNovo == StatusVenda.Entregue)
                return true;

            return false;
        }

        
        [HttpPost("FinalizarVenda")]
        public IActionResult FinalizarVenda(Venda venda)
        {
            var consultaVendedor = _context.Vendedores.Find(venda.IdVendedor);
            if(consultaVendedor == null)
                return BadRequest("Vendedor não cadastrado");

            var consultaProduto = _context.Produtos.Find(venda.IdProduto);
            if (consultaProduto == null)
                return BadRequest("Produto não cadastrado");


            _context.Add(venda);
            _context.SaveChanges();       
            return Ok(venda);
        }

        [HttpGet("BuscarVenda{id}")]
        public IActionResult BuscarVenda(int id)
        {
            
            var resultado = _context.Vendas.Find(id);                  
            return Ok(resultado);
        }


      [HttpPut("{id}")]
        public IActionResult AtualizarVenda(int id, StatusVenda statusVenda)
        {
            var dbVenda = _context.Vendas.Find(id);
            if (dbVenda == null)
                return NotFound("Venda não encontrada!");

            if (!podeAtualizar(dbVenda.StatusVenda, statusVenda))
                return BadRequest("Status inválido para essa venda!");

            dbVenda.StatusVenda = statusVenda;

            _context.Vendas.Update(dbVenda);
            _context.SaveChanges();

            return Ok();
        }


        
    }
}