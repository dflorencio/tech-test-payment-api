using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProdutoController : ControllerBase
    {
        
        private readonly ApiContext _contextProduto;

        public ProdutoController( ApiContext ctx )
        {
            _contextProduto = ctx;
        }

        [HttpPost("CadastrarProduto")]
        public IActionResult CastrarProduto(Produto produto)
        {
            _contextProduto.Add(produto);
            _contextProduto.SaveChanges();
            return Ok(produto);
        }

        [HttpGet("ConsultarCadPro")]
        public IActionResult ConsultaProduto()
        {
            
            var id = _contextProduto.Produtos.ToList();
            return Ok(id);
        }
    }
}